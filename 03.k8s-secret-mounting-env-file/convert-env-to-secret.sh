#!/bin/sh

touch secret.yaml;
env=$(base64 -w 0 .env)

echo "---" > secret.yaml
echo "apiVersion: v1" >> secret.yaml
echo "data:" >> secret.yaml
echo "  .env: $env" >> secret.yaml
echo "kind: Secret" >> secret.yaml
echo "metadata:" >> secret.yaml
echo "  name: env-file-secret" >> secret.yaml
echo "  namespace: mytc" >> secret.yaml
echo "type: Opaque" >> secret.yaml

echo "Done!"