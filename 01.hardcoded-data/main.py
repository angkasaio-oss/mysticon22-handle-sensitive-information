import os
import time
from flask import Flask, request, jsonify

app = Flask(__name__)

# HARDCODED SENSITIVE INFORMATION
API_USER = 'mysticon22'
API_PASSWORD = 'myPassword123'

@app.route('/')
def hello():
    return jsonify(
        {
            'API_USER' : API_USER,
            'API_PASSWORD' : API_PASSWORD
        }
    )
if __name__ == '__main__':
    print("APP USE DEVELOPMENT SERVER ON 8080:")
    app.run(host='0.0.0.0', port=8080, debug=True)


